<?php

//Create a function named printDivisibleOfFive that will perform the following:
    // -Using loops, print all numbers that are divisible by 5 from 0 to 1000.
    // -Stop the loop when the loop reaches ts 100th iteration.
    // -Invoke the function in the index.php

    function printDivisibleOfFive() {

        for($count = 0; $count <=1000; $count++) {

            if($count %5 !== 0) {
                continue;
            }
    
            echo $count . ' ';
    
            if($count >= 100) {
                break;
            }
    
        }
    }

    //Create an empty array named students.
    $students = [];