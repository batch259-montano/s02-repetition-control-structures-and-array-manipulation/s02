<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
    <h2>Divisible of Five</h2>
    <?php printDivisibleOfFive(); ?>

    <!-- Perform the following array functions: -->

        <h3>Array Manipulation</h3>
       <!-- - Accept a name of the student and add in to the students array.-->   
        <?php array_unshift($students,'John Smith'); ?>    

       <!-- - Print the names added so far in the students array. --> 
       <pre> <?php var_dump($students); ?> </pre>

       <!-- - Count the number of names in the students array. -->
       <pre> <?php echo count($students); ?> </pre>

       <!-- - Add another student then print the array and its new count. -->
       <?php array_push($students,'Jane Smith'); ?> 
       <pre> <?php var_dump($students); ?> </pre>
       <pre> <?php echo count($students); ?> </pre>

       <!-- -Finally, remove the first student and print the array and its count. -->
       <?php array_shift($students); ?>
       <pre> <?php var_dump($students); ?> </pre>
       <pre> <?php echo count($students); ?> </pre>
       
</body>
</html>